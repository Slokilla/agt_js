let searchBar = document.getElementById("searchBar");
let results = document.getElementById("results");
let xhr = new XMLHttpRequest();
let selectedRank = -1;

/**
 * A chaque entrée dans le champs, on vérifie qu'il n'y a pas déjà une connexion ouverte.
 * Si c'est le cas, on la ferme.
 * Sinon on ouvre la connexion, et on poste la recherche sous la forme
 * "s=RECHERCHE".
 * Quand on a une réponse, on vide la liste des recommandations actuelles,
 * et on la nourris avec la liste qu'on vient de récupérer.
 */
searchBar.addEventListener("input", ev => {
    if (xhr.OPENED){
        xhr.abort();
    }

    xhr.open("POST", "serveur/server.php", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    
    xhr.addEventListener("load", e => {
        let towns = e.target.response.split("|");
        results.innerHTML = "";

        towns.forEach(el => {
            let temp = document.createElement("div");

            temp.innerText = el;
            temp.onclick = function(){
                validSelection(temp);
            };
            temp.onmouseover = function(){
                setSelected(temp);
                selectedRank = Array.prototype.indexOf.call(results.children, temp);
            };

            results.appendChild(temp);
        });

        selectedRank = -1;
    });

    xhr.send("s=" + ev.target.value);
})

/**
 * Event lié à l'appuie sur une touche.
 * On traite trois touches : haut; bas; entrée.
 * on parcours les noeud enfants de la div résultat.
 */
searchBar.addEventListener("keyup", ev => {
    let selected;

    switch (ev.code) {
        case "ArrowUp":
            ev.preventDefault();

            if (selectedRank === -1) {
                selectedRank = results.children.length-1;
            } else {
                selectedRank = (selectedRank + results.children.length-1) % (results.children.length);
            }

            setSelected(results.childNodes[selectedRank]);
            break;
        case "ArrowDown":
            if (selectedRank === -1) {
                selectedRank = 0;
            } else {
                selectedRank = (selectedRank + 1) % (results.children.length);
            }
            setSelected(results.childNodes[selectedRank]);
            break;
        case "Enter":
            if (selectedRank !== -1) {
                validSelection(results.childNodes[selectedRank]);
            }

            break;
        default:
            break;
    }
}, false);

function validSelection(target) {
        searchBar.value = target.textContent;
        results.innerHTML = "";
}

function setSelected(target) {
    if(document.querySelector(".selected"))
        document.querySelector(".selected").classList.remove("selected");
    
    target.classList.add("selected");
}