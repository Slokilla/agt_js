<?php

if ($content = file_get_contents("towns.txt")) {
    $content = unserialize($content, array(true));
    
    if ($_POST["s"]) {
        $reg = "/^" . $_POST["s"] . "[\s\S]*/i";
        $result = preg_grep($reg, $content);
        echo implode("|", $result);
    }
} else {
    header("HTTP/1.0 404 Not Found");
    exit;
}

