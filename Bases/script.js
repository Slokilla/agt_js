let toConvert = document.getElementById("toConvert");
let chiffresLettres = [
    "un",
    "deux",
    "trois",
    "quatre",
    "cinq",
    "six",
    "sept",
    "huit",
    "neuf",
];
let dizaineLettres = [
    "dix",
    "vingt",
    "trente",
    "quarante",
    "cinquante",
    "soixante",
    "soixante-dix",
    "quatre-vingt",
    "quatre-vingt-dix",
];
let specialLettres = [
    "onze",
    "douze",
    "treize",
    "quatorze",
    "quinze",
    "seize"
];

/**
 * Fonction qui converti un nombre en chiffre.
 * Elle commence par récupérer les chiffre des centaines, dizaines, unités
 * puis les envoie à la fonction ecrireCentaines();
 * @param nbr le nombre à convertir
 */
function convert(nbr) {
    let unites, dizaines, centaines;
    let result = "";
    unites = nbr % 10;
    dizaines = (nbr % 100 - unites) / 10;
    centaines = (nbr - unites - dizaines * 10) / 100;

    if (centaines) {
        result = ecrireCentaines(centaines, dizaines, unites);
    } else if (dizaines) {
        result = ecrireDizaine(dizaines, unites);
    } else {
        result = ecrireUnite(unites);
    }

    console.log(result);
}

/**
 * La fonction qui écrit les centaines.
 * Si le nombre de centaines est 1, il est inutile d'écrire "un cent",
 * donc on saute l'étape d'écriture.
 * Sans dizaine ni unités derrière, les centaines s'accordent en nombre
 * @param c le chiffre des centaines du nombre à convertir
 * @param d le chiffre des dizaines du nombre à convertir
 * @param u le chiffre des unités du nombre à convertir
 * @returns {string} la chaines finale
 */
function ecrireCentaines(c, d, u) {
    let localResult = "";

    if (c !== 1) {
        localResult += chiffresLettres[c - 1] + "-";
    }

    localResult += "cent";

    if (!d && !u && c > 1) {
        localResult += "s";
    } else if (d || u) {
        localResult += "-" + ecrireDizaine(d, u);
    }

    return localResult ? localResult : "";
}

/**
 * La fonction qui écrit les centaines.
 * Si le nombre de dizaines est 7 ou 9, le traitement peut être particulier.
 * En effet, après 7 et 9 on trouve une prononciation de dizaines.
 * Dans le cas ou le nombre d'unité est 1, on rajoute un "et".
 * @param d le chiffre des dizaines du nombre à convertir
 * @param u le chiffre des unités du nombre à convertir
 * @returns {string} la chaine correspondant aux dizaines et aux unités
 */
function ecrireDizaine(d, u) {
    let localResult = "";

    if ((d === 7 || d === 9) && [1, 2, 3, 4, 5, 6].includes(u)) {
        localResult += dizaineLettres[d - 2];

        if (u === 1) {
            localResult += "-et-";
        } else {
            localResult += "-";
        }

        localResult += specialLettres[u - 1];
    } else if (d === 1 && [1, 2, 3, 4, 5, 6].includes(u)) {
        localResult += specialLettres[u - 1];
    } else {
        if (d !== 0) {
            localResult += dizaineLettres[d - 1];

            if (u === 1 && d > 1) {
                localResult += "-et-" + ecrireUnite(u);
            } else if (u) {
                localResult += "-" + ecrireUnite(u);
            } else if (d === 8) {
                localResult += "s";
            }
        } else {
            localResult += ecrireUnite(u);
        }
    }
    
    return localResult ? localResult : "";
}

/**
 * Écriture du chiffre des unités,
 * @param u le chiffre à écrire
 * @returns {string} le chiffre des unité converti
 */
function ecrireUnite(u) {
    return chiffresLettres[u - 1] ? chiffresLettres[u - 1] : "";
}

/**
 * Point d'entrée.
 * Déclenché quand la valeur de l'input change.
 */
toConvert.addEventListener("input", e => {
    if (e.target.value) {
        convert(e.target.value);
    }
});