let nom = document.getElementById("nom");
let prenom = document.getElementById("prenom");
let sexe = document.getElementById("sexe");
let sexeM = document.getElementById("sexeM");
let sexeF = document.getElementById("sexeF");
let sexeA = document.getElementById("sexeA");
let age = document.getElementById("age");
let pseudo = document.getElementById("pseudo");
let pass = document.getElementById("pass");
let passVerif = document.getElementById("passVerif");
let pays = document.getElementById("pays");
let newsletter = document.getElementById("newsletter");
let submitBtn = document.getElementById("submitBtn");
let erreurNom = document.getElementById("erreurNom");
let erreurPrenom = document.getElementById("erreurPrenom");
let erreurSexe = document.getElementById("erreurSexe");
let erreurAge = document.getElementById("erreurAge");
let erreurPseudo = document.getElementById("erreurPseudo");
let erreurMdp = document.getElementById("erreurMdp");
let erreurVerifMdp = document.getElementById("erreurVerifMdp");

function errorColor(champs, message, slot) {
    champs.style.borderColor = "#f00";
    champs.style.backgroundColor = "#fed";
    console.log(message);
    slot.textContent = message;
}

function validColor(champs, slot) {
    champs.style.borderColor = "#0f0";
    champs.style.backgroundColor = "#dfd";
    if (slot.textContent) {
        slot.textContent = "";
    }
}

function verifLengthText(champs, length, slot) {
    console.log(champs.value.length);
    if (!champs.value) {
        message = "Le " + champs.labels[0].textContent + " ne peut pas être vide et doit faire plus de " + length + " caractères.";
        errorColor(champs, message, slot);
        return false
    } else if (champs.value.length < length) {
        message = "Le " + champs.labels[0].textContent + " doit faire plus de " + length + " caractères.";
        errorColor(champs, message, slot);
        return false;
    }
    validColor(champs, slot);
    return true;
}

function verifSexe() {
    if (sexeM.checked === true || sexeF.checked === true || sexeA.checked === true) {
        validColor(sexe, erreurSexe);
        return true;
    }
    errorColor(sexe, "Veuillez selectionner un sexe.", erreurSexe);
    return false;
}

function verifAge(champs) {
    if (champs.value < 5 || champs.value > 140) {
        errorColor(champs, "Votre âge doit être compris entre 5 et 140 ans.", erreurAge);
        return false;
    } else if (!champs.value) {
        errorColor(champs, "Veuillez rentrer un âge compris entre 5 et 140 ans.", erreurAge);
        return false;
    }
    validColor(champs, erreurAge);
    return true;
}

function verifPassword(champs, champsVerif, slot) {
    if (champs.value !== champsVerif.value || !verifLengthText(champsVerif, 6, slot)) {
        errorColor(champsVerif, "Les mots de passe ne sont pas identiques.", erreurVerifMdp);
        return false;
    }
    validColor(champsVerif, slot);
    return true;
}


nom.addEventListener("input", ev => verifLengthText(ev.target, 2, erreurNom));
prenom.addEventListener("input", ev => verifLengthText(ev.target, 2, erreurPrenom));
pseudo.addEventListener("input", ev => verifLengthText(ev.target, 4, erreurPseudo));
pass.addEventListener("input", ev => verifLengthText(ev.target, 6, erreurMdp));
passVerif.addEventListener("input", ev => verifPassword(pass, ev.target, erreurVerifMdp));
age.addEventListener("input", ev => verifAge(age));

submitBtn.addEventListener("click", ev => {
    const a = verifLengthText(nom, 2, erreurNom);
    const b = verifLengthText(prenom, 2, erreurPrenom);
    const c = verifLengthText(pseudo, 4, erreurPseudo);
    const d = verifLengthText(pass, 6, erreurMdp);
    const e = verifPassword(pass, passVerif, erreurVerifMdp);
    const f = verifAge(age);
    const g = verifSexe();

    if (a && b && c && d && e && f && g) {
        alert("Le formulaire est correct, merci.")
    }
});